import 'dart:io';

import 'package:calculator/calculator.dart';

void main(List<String> arguments) {
  print("1.Add\n"
  "2.Subtract\n"
  "3.Multiple\n"
  "4.Divide\n"
  "5.Modular\n"
  "6.Power\n"
  "Choose Math Operator(Input Number)");
  int op = int.parse(stdin.readLineSync()!);
  print("Enter frist number: ");
  double a = double.parse(stdin.readLineSync()!);
  print("Enter second number: ");
  double b = double.parse(stdin.readLineSync()!);
  Calculator cal = Calculator(a, b, op);
  cal.process();
}

class Calculator{
  double a;
  double b;
  int op;

  Calculator(this.a, this.b, this.op);

  void process(){
    double? ans;
    switch(op){
      case 1:{
        ans=add();
      }
      break;
      case 2:{
        ans=sub();
      }
      break;
      case 3:{
        ans=mul();
      }
      break;
      case 4:{
        ans=divide();
      }
      break;
      case 5:{
        ans=mod();
      }
      break;
      case 6:{
        ans=pow();
      }
      break;
    }
    print("Answer: "
    "$ans");
  }

  double add(){
    return a+b;
  }

  double sub(){
    return a-b;
  }

  double mul(){
    return a*b;
  }

  double divide(){
    return a/b;
  }

  double mod(){
    return a%b;
  }

  double pow(){
    if(b==0){
      return 1.00;
    }
    double answer = 1;
    for(int i=1; i<=this.b; i++){
      answer *= a;
    }
    return answer;
  }
}